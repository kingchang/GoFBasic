/*
 * Copyright 2016 TeddySoft Technology. All rights reserved.
 * 
 */
package tw.teddysoft.gof.Command.exercise;
import org.junit.Test;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

public class WindowCommandTest {
	class MockRemoteWindow extends Window {
		private boolean broken;
		private boolean open;

		public MockRemoteWindow(String ipAddress) {
			super(ipAddress);
		}
		public MockRemoteWindow(String ipAddress, boolean broken,boolean open) {
			super(ipAddress);
			this.broken = broken;
			this.open = open;
		}




		@Override
		public boolean isBroken() {
			return broken;
		}

		@Override
		public boolean isOpen() {
			return open;
		}
	}

	@Test
	public void testCommand_WindowOpen() {
		Window window = new MockRemoteWindow("192.168.0.1", false,true);
		Command windowCommand = new WindowCommand(window);
		Result result = windowCommand.execute();
		assertEquals(Status.CRITICAL, result.getStatus());
		assertTrue(result.getMessage().startsWith("窗戶被開啟"));
	}
	@Test
	public void testCommand_WindowClose() {
		Window window = new MockRemoteWindow("192.168.0.1", false,false);
		Command windowCommand = new WindowCommand(window);
		Result result = windowCommand.execute();
		assertEquals(Status.OK, result.getStatus());
		assertTrue(result.getMessage().isEmpty());
	}


	@Test
	public void testCommand_WindowBroken() {
		Window window = new MockRemoteWindow("192.168.0.1", true,false);
		Command windowCommand = new WindowCommand(window);
		Result result = windowCommand.execute();
		assertEquals(Status.CRITICAL, result.getStatus());
		assertTrue(result.getMessage().startsWith("窗戶被打破"));
	}

}



