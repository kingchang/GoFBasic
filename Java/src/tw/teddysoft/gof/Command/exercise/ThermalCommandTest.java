package tw.teddysoft.gof.Command.exercise;

import org.junit.Test;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

public class ThermalCommandTest {

  class MockRemoteThermal extends Thermal {
    private boolean overheat;

    public MockRemoteThermal(String ipAddress) {
      super(ipAddress);
    }
    public MockRemoteThermal(String ipAddress, boolean overheat) {
      super(ipAddress);
      this.overheat = overheat;
    }


    @Override
    public boolean isOverheat() {
      return overheat;
    }
  }

  @Test
  public void testCommand_ThermalOverheat() {
    Thermal window = new MockRemoteThermal("192.168.0.1",true);
    Command thermalCommand = new ThermalCommand(window);
    Result result = thermalCommand.execute();
    assertEquals(Status.CRITICAL, result.getStatus());
    assertTrue(result.getMessage().startsWith("溫度過熱"));
  }
  @Test
  public void testCommand_ThermalNormal() {
    Thermal window = new MockRemoteThermal("192.168.0.1",false);
    Command thermalCommand = new ThermalCommand(window);
    Result result = thermalCommand.execute();
    assertEquals(Status.OK, result.getStatus());
    assertTrue(result.getMessage().isEmpty());
  }



}