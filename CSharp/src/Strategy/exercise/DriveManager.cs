﻿/*
 * Copyright 2017 TeddySoft Technology. 
 * 
 */
using System;

namespace Tw.Teddysoft.Gof.Strategy.Exercise
{
    public class DriveManager
    {
        public void format(FileSystemEnum fileSystem)
        {
            switch (fileSystem)
            {
                case FileSystemEnum.NTFS:
                    formatNtfs();
                    break;
                case FileSystemEnum.FAT32:
                    formatFat32();
                    break;
                case FileSystemEnum.FAT:
                    formatFat();
                    break;
            }
        }
        private void formatNtfs() { }
        private void formatFat32() { }
        private void formatFat() { }
    }
}
